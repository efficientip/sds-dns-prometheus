# -*- Mode: Python; python-indent-offset: 4 -*-
#
# Time-stamp: <2019-09-08 15:03:32 alex>
#
# --------------------------------------------------------------------
# lab-prometheus - metrics for prometheus
# Copyright (C) 2016-2017  Alexandre Chauvin Hameau <ach@meta-x.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# --------------------------------------------------------------------

"""
"""

from prometheus_client import (Counter, Gauge, Info)
import pprint
import logging

class dnsStats(object):
    DNS_METRICS = {
        "standard": {},
        "range": {}
    }

    registry = None

    def __init__(self, registry):
        self.registry = registry

        self.DNS_METRICS["version"] = Info('dns_version', 
                                           'Version of DNS server',
                                           registry=self.registry)

    def addCategory(self, category, type='standard'):
        if type=='standard':
            if category not in self.DNS_METRICS['standard']:
                self.DNS_METRICS['standard'][category] = {}
        if type=='range':
            if category not in self.DNS_METRICS['range']:
                self.DNS_METRICS['range'][category] = {}


    def normalize(self, s):
        s = s.replace("-","_")
        s = s.replace("+","p")
        s = s.replace("!","not_")
        s = s.replace("#","h")
        return(s)

    def addStandardGaugeValue(self, k, section, tree):
        kn = self.normalize(k)
        name = "{}_{}".format(section, kn)
        
        if kn not in self.DNS_METRICS['standard'][section]:
            self.DNS_METRICS['standard'][section][kn] = Gauge(name, 
                                                              name,
                                                              registry = self.registry)
        self.DNS_METRICS['standard'][section][kn].set(tree[section][k])

    def analyze(self, tree):
        # dns version
        self.DNS_METRICS['version'].info({'bind': tree['version']})

        for section in self.DNS_METRICS['standard'].keys():
            try:
              for k in tree[section]:
                kn = self.normalize(k)
                name = "{}_{}".format(section, kn)

                if section=='opcodes' and k.startswith('RESERVED'):
                    continue

                if section=='rcodes' and k.startswith('RESERVED'):
                    continue

                if section=='memory' and k.startswith('contexts'):
                    continue

                if section=='taskmgr':
                    if k == 'tasks':
                        continue
                    if kn == "thread_model":
                        if kn not in self.DNS_METRICS['standard']['taskmgr']:
                            self.DNS_METRICS['standard']['taskmgr'][kn] = Info(name, name)
                            self.DNS_METRICS['standard']['taskmgr'][kn].info({'model': tree[section][k]})
                        continue

                if section == "views":
                    for section1 in tree[section]:
                        if section1 == "_default":
                            for section2 in tree[section][section1]:
                                if section2 == 'resolver':
                                    for section3 in tree[section][section1][section2]:
                                        for s3k in tree[section][section1][section2][section3]:
                                            s3kn = self.normalize(s3k)
                                            name = "views_"+s3kn
                                            kn = name
                                            if kn not in self.DNS_METRICS['standard'][section]:
                                                self.DNS_METRICS['standard'][section][kn] = Gauge(name, 
                                                                                                  name,
                                                                                                  ['view', 'type', 'subtype'],
                                                                                                  registry = self.registry)
                                            self.DNS_METRICS['standard'][section][kn].labels("default", "resolver", section3).set(tree[section][section1][section2][section3][s3k])

                    continue

                self.addStandardGaugeValue(k, section, tree)
            except KeyError:
                print(f"{section} not found or invalid.")

        for section in self.DNS_METRICS['range'].keys():
            for k in tree[section]:
                kn = self.normalize(k)
                name = "{}_{}".format(section, kn)

                if k not in self.DNS_METRICS['range'][section]:
                    self.DNS_METRICS['range'][section][k] = Gauge(name, 
                                                                  name,
                                                                  registry = self.registry,
                                                                  labelnames = ['range'])
                for r in tree[section][k]:
                    self.DNS_METRICS['range'][section][k].labels(r).set(tree[section][k][r])
