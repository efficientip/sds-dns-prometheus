# Purpose

This package demonstrate statistics gathered from the SOLIDserver DNS implementation in Grafana. It uses a translator for presenting raw statistics to Prometheus and Grafana for presenting metrics in a build-in dashboard.

# Installation

## add global DNS configuration on the SOLIDserver
in the file ```/etc/namedb/global_include.conf```, add:

```
statistics-channels {
     inet 192.168.56.254 port 18080 allow { 192.168.56.103; };
};
```
allowing the Prometheus translator server (192.168.56.103) to get access through the SOLIDserver interface (192.168.56.254) to the statistics channels listening on the TCP port (18080). This configuration needs to be reflected in the firewall rules of the SOLIDserver, you can add a rule at the end of the list.

## edit the SOLIDserver information in the ```Dockerfile-pyDNS``` file

set SDS_HOST, SDS_PORT accordingly to your configuration

## create container for the translator

```docker build -f Dockerfile-pyDNS -t sds-dns-prometheus .```

# Start engine

using docker compose, start the 3 components: translator, grafana, prometheus. Adapt the configuration with regards to your environment.

```docker-compose up```

# Check translation service

Once started, the sds-dns-prometheus container is proposing a text version for prometheus of the DNS statistics. You can check the content on the docker host using your browser on the port 5000 (eg ```http://192.168.16.205:5000/metrics```).

# Configure Grafana

## connect on console

log on ```http://192.168.16.205:3000/login``` with ```admin``` account using default credentials ```admin```

## add the data source
in the datasource menu, add a prometheus source pointing to ```http://prometheus:9090```

## add a dashboard

in the dashboard mange section, import the proposed dashboard by cut/paste from the json file ```grafana-dns.json```

# have fun
and let us know...
