# -*- Mode: Python; python-indent-offset: 4 -*-
#
# Time-stamp: <2019-09-08 10:28:57 alex>
#
# --------------------------------------------------------------------
# lab-prometheus - metrics for prometheus
# Copyright (C) 2016-2017  Alexandre Chauvin Hameau <ach@meta-x.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# --------------------------------------------------------------------

"""
 Prometheus metrics class to store all the metrics, metric flask
 endpoint and decorator for url calls counting.
"""

import os, sys
import json
import urllib3
import logging
import time
import pprint

from prometheus_client import (Counter,
                               Gauge,
                               generate_latest,
                               CONTENT_TYPE_LATEST,
                               CollectorRegistry)
from flask import Response, request
from ws_app import app
from dns import dnsStats

http = urllib3.PoolManager()
lastUrlFetch = 0

# Create a metric to track time spent and requests made.

registry = CollectorRegistry()

dns = dnsStats(registry)

SDS_HOST = os.environ.get('SDS_HOST', "192.168.56.254")
SDS_PORT = os.environ.get('SDS_PORT', "18080")
BINDSTATS_URL = "http://%s:%s/json" % (SDS_HOST, SDS_PORT)

FETCH_CACHE_DELAY = int(os.environ.get('CACHE_DELAY', "5"))


def getJSONStatsFromBIND(url):
    """Return a json structure from the BIND9 statistics obtained at the
       given URL"""

    r = http.request('GET', url)
    if r.status != 200:
        logging.error("cannot retrieve stats")
        return

    try:
        return json.loads(r.data.decode("utf-8"))
    except:
        logging.error("cannot read json flow")
        exit()


@app.route('/metrics')
def metrics():
    """Flask endpoint to gather the metrics, will be called by Prometheus."""

    global lastUrlFetch
    global FETCH_CACHE_DELAY

    if time.time() - lastUrlFetch > FETCH_CACHE_DELAY:
        tree = getJSONStatsFromBIND(BINDSTATS_URL)
        # pprint.pprint(tree)
        lastUrlFetch = time.time()
        dns.analyze(tree)

    # getJSONStatsFromBIND.cache_clear()
    # print(getJSONStatsFromBIND.cache_info())

    return Response(generate_latest(registry),
                    mimetype=CONTENT_TYPE_LATEST)
