"""
"""

import os, sys

import logging
import functools

from ws_app import app
from flask import make_response, jsonify
import metrics

import pprint

metrics.dns.addCategory("opcodes")
metrics.dns.addCategory("nsstats")
metrics.dns.addCategory("qtypes")
metrics.dns.addCategory("rcodes")
metrics.dns.addCategory("resstats")
metrics.dns.addCategory("sockstats")
metrics.dns.addCategory("traffic", type='range')
metrics.dns.addCategory("memory")
metrics.dns.addCategory("taskmgr")
metrics.dns.addCategory("views")

if __name__ == '__main__':
    app.secret_key = "not_so_secret"
    app.debug = True
    app.run(host='0.0.0.0')
